module gitlab.com/gomidi/midiproxy

go 1.14

require (
	gitlab.com/gomidi/midi v1.15.3
	gitlab.com/gomidi/midiline v0.16.0
)
