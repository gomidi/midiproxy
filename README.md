# midiproxy

[![Build Status Travis/Linux](https://travis-ci.org/gomidi/midiproxy.svg?branch=master)](http://travis-ci.org/gomidi/midiproxy) [![Documentation](http://godoc.org/github.com/gomidi/midiproxy?status.png)](http://godoc.org/github.com/gomidi/midiproxy)

`midiproxy` is a tool to connect a MIDI in port with an MIDI out port an allow manipulation of the MIDI data flowing from
in to out. The manipulation is done with the help of a midiline (see https://github.com/gomidi/midiline) and
`midiproxy` provides a UI for manipulating and running of such midilines.

## Status

proof of concept 

## Installation

```sh
go get -d github.com/gomidi/midiproxy/...
go install github.com/gomidi/midiproxy/cmd/midiproxy
```
## Run

```sh
midiproxy ui
```