package main

import (
	"fmt"
	"sort"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type ConditionScreen struct {
	*tview.Box
	currentCondition int
	Conditions       []string
}

func newConditionScreen() *ConditionScreen {
	return &ConditionScreen{Box: tview.NewBox().SetBorder(true).SetTitle("conditions"), Conditions: getConditions(), currentCondition: -1}
}

func (m *ConditionScreen) Draw(screen tcell.Screen) {
	m.Box.Draw(screen)
	x, y, width, height := m.GetInnerRect()

	radioButton := "( )" // Unchecked.
	if m.currentCondition == -1 {
		radioButton = "[red](X)" // Checked.
	}
	line := fmt.Sprintf(`%s %s`, radioButton, "NEW")
	tview.Print(screen, line, x+1, y, width, tview.AlignLeft, tcell.ColorYellow)

	for index, option := range m.Conditions {
		if index >= height-1 {
			break
		}
		radioButton := "( )" // Unchecked.
		if index == m.currentCondition {
			radioButton = "[red](X)" // Checked.
		}
		line := fmt.Sprintf(`%s %s`, radioButton, option)
		tview.Print(screen, line, x+2, y+index+1, width, tview.AlignLeft, tcell.ColorYellow)
	}
}

func (m *ConditionScreen) showForm() {
	form := ConditionForm(m.currentConditionName())
	pagesRight.RemovePage("form")
	pagesRight.AddAndSwitchToPage("form", form, true)
}

func (m *ConditionScreen) Focus(delegate func(p tview.Primitive)) {
	m.Conditions = getConditions()
	sort.Strings(m.Conditions)
	m.showForm()
}

func (m *ConditionScreen) currentConditionName() string {
	mt := ""
	if m.currentCondition >= 0 {
		mt = m.Conditions[m.currentCondition]
	}
	return mt
}

// InputHandler returns the handler for this primitive.
func (m *ConditionScreen) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	//	return m.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	//		panic(event.Key())
	return func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		switch event.Key() {
		case tcell.KeyDelete:
			name := m.currentConditionName()
			if name != "" {
				removeCondition(name)
				m.Conditions = getConditions()
				sort.Strings(m.Conditions)
				m.currentCondition = -1
				m.showForm()
				//app.Refresh()
			}
		case tcell.KeyUp:
			m.currentCondition--
			if m.currentCondition < -1 {
				m.currentCondition = -1
			}
			m.showForm()
		case tcell.KeyDown:
			m.currentCondition++
			if m.currentCondition >= len(m.Conditions) {
				m.currentCondition = len(m.Conditions) - 1
			}
			m.showForm()
		}
	}
	//	})
}
