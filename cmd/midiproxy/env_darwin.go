// +build darwin

package main

/*
according to http://wiki.freepascal.org/Multiplatform_Programming_Guide#Configuration_files
*/

import (
	"os"
	"path/filepath"
)

func setUserDir() {
	home := os.Getenv("HOME")
	if home == "" {
		home = filepath.Join("/home", os.Getenv("USER"))
	}
	USER_DIR = filepath.Join(home + "..midiproxy")
}

func init() {
	setUserDir()
}
