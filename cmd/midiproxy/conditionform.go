package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/rivo/tview"
	lineconfig "gitlab.com/gomidi/midiline/config"
)

func ConditionFormAdd() (form *tview.Form) {
	form = tview.NewForm()
	var m lineconfig.Condition
	makers := getConditionMakers()

	var _makers = make([]string, len(makers))

	for i, mk := range makers {
		_makers[i] = mk[0] + "(" + tview.Escape(mk[1]) + ")"
	}

	form.
		AddInputField("name", "", 100, nil, func(text string) {
			m.Name = text
		}).
		AddDropDown("make", _makers, -1, func(option string, optionIndex int) {
			for i, fn := range makers {
				if i == optionIndex {
					m.Make = fn[0]
				}
			}
		}).
		AddInputField("parameters (comma separated)", "", 150, func(textToCheck string, lastChar rune) bool {
			/*
				var args []interface{}
				err := json.Unmarshal( []byte("["+textToCheck+"]"), &args)
				return err == nil
			*/
			return true
		}, func(text string) {
			var args []interface{}
			err := json.Unmarshal([]byte("["+text+"]"), &args)

			/*
				if err != nil {
					showError(fmt.Errorf("ERROR in parameters: %s", err.Error()))
					return
				}
			*/
			if err == nil {
				m.Params = args
			}
		}).
		AddButton("add", func() {
			if m.Name == "" {
				showError(fmt.Errorf("empty name is not allowed"))
				return
			}
			if m.Make == "" {
				showError(fmt.Errorf("empty make is not allowed"))
				return
			}
			addCondition(m)
			pages.SwitchToPage("Condition")
			app.SetFocus(pages)
		}).
		SetCancelFunc(func() {
			pages.SwitchToPage("Condition")
			app.SetFocus(pages)
			app.SetFocus(pagesRight)
		})

	form.SetBorder(true).SetTitle("new condition").SetTitleAlign(tview.AlignLeft)
	return form
}

func ConditionFormEdit(selectedCondition string) (form *tview.Form) {
	form = tview.NewForm()
	var mt lineconfig.Condition
	var selected int = -1
	for i, m := range data.Conditions {
		if m.Name == selectedCondition {
			selected = i
			mt = m
		}
	}

	if selected == -1 {
		showError(fmt.Errorf("unknown condition %#v", selectedCondition))
		return ConditionFormAdd()
	}

	var args []string

	for i, arg := range mt.Params {
		bt, err := json.Marshal(arg)
		if err != nil {
			showError(fmt.Errorf("can't json marshal argument %v of condition %#v: %v", i, selectedCondition, err.Error()))
			return
		}

		args = append(args, fmt.Sprintf("%s", string(bt)))
	}

	var selectedMake = -1
	makers := getConditionMakers()

	var _makers = make([]string, len(makers))

	for i, mk := range makers {
		_makers[i] = mk[0] + "(" + tview.Escape(mk[1]) + ")"
	}

	for i, f := range makers {
		if f[0] == mt.Make {
			selectedMake = i
		}
	}
	var oldName = mt.Name
	_ = oldName
	form.
		/*
			AddInputField("Name", mt.Name, 100, nil, func(text string) {
				mt.Name = text
			}).
		*/
		AddDropDown("make", _makers, selectedMake, func(option string, optionIndex int) {
			for i, f := range makers {
				if i == optionIndex {
					mt.Make = f[0]
				}
			}
		}).
		AddInputField("parameters (comma separated)", strings.Join(args, ","), 150, func(textToCheck string, lastChar rune) bool {
			/*
				var args []interface{}
				err := json.Unmarshal( []byte("["+textToCheck+"]"), &args)
				return err == nil
			*/
			return true
		}, func(text string) {
			var args []interface{}
			err := json.Unmarshal([]byte("["+text+"]"), &args)
			/*
				if err != nil {
					showError(fmt.Errorf("ERROR in parameters: %s", err.Error()))
					return
				}
			*/
			if err == nil {
				mt.Params = args
			}
		}).
		AddButton("save", func() {
			replaceCondition(oldName, mt)
			pages.SwitchToPage("Condition")
			app.SetFocus(pages)
		}).
		SetCancelFunc(func() {
			pages.SwitchToPage("Condition")
			app.SetFocus(pages)
			app.SetFocus(pagesRight)
		})

	form.SetBorder(true).SetTitle(fmt.Sprintf("edit condition %#v", selectedCondition)).SetTitleAlign(tview.AlignLeft)
	return form
}

// edit or create a Condition
func ConditionForm(selectedCondition string) (form *tview.Form) {
	if selectedCondition == "" {
		return ConditionFormAdd()
	}
	return ConditionFormEdit(selectedCondition)
}
