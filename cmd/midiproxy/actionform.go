package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/rivo/tview"
	lineconfig "gitlab.com/gomidi/midiline/config"
)

func actionFormAdd() (form *tview.Form) {
	form = tview.NewForm()
	var trafo lineconfig.Action
	//	var newFunc = ""
	//	var newargs []interface{}
	//	var newname = ""
	makers := getActionMakers()

	var _makers = make([]string, len(makers))

	for i, mk := range makers {
		_makers[i] = mk[0] + "(" + tview.Escape(mk[1]) + ")"
	}

	form.
		AddInputField("name", "", 100, nil, func(text string) {
			trafo.Name = text
		}).
		AddDropDown("make", _makers, -1, func(option string, optionIndex int) {
			for i, fn := range makers {
				if i == optionIndex {
					trafo.Make = fn[0]
				}
			}
		}).
		AddInputField("parameters (comma separated)", "", 150, func(textToCheck string, lastChar rune) bool {
			/*
				var args []interface{}
				err := json.Unmarshal( []byte("["+textToCheck+"]"), &args)
				return err == nil
			*/
			return true
		}, func(text string) {
			var args []interface{}
			err := json.Unmarshal([]byte("["+text+"]"), &args)
			if err == nil {
				trafo.Params = args
			}
		}).
		AddButton("add", func() {
			if trafo.Name == "" {
				showError(fmt.Errorf("name may not be empty"))
				return
			}
			if trafo.Make == "" {
				showError(fmt.Errorf("make may not be empty"))
				return
			}
			addAction(trafo)
			pages.SwitchToPage("action")
			app.SetFocus(pages)
			//			pagesRight.RemovePage("form")
			//			app.SetFocus(pages)
			//				changeScreen(newTransformerScreen())
			//changeScreen(runnerPage())
		}).SetCancelFunc(func() {
		pages.SwitchToPage("action")
		app.SetFocus(pages)
		app.SetFocus(pagesRight)
	})
	form.SetBorder(true).SetTitle("new action").SetTitleAlign(tview.AlignLeft)
	return form
}

func actionFormEdit(selectedAction string) (form *tview.Form) {
	form = tview.NewForm()
	var mt lineconfig.Action
	var selected int = -1
	for i, m := range data.Actions {
		if m.Name == selectedAction {
			selected = i
			mt = m
		}
	}

	if selected == -1 {
		showError(fmt.Errorf("unknown action: %#v", selectedAction))
		return actionFormAdd()
		//		panic("unknown matcher " + selectedTransformer)
	}

	var args []string

	for i, arg := range mt.Params {
		bt, err := json.Marshal(arg)
		if err != nil {
			showError(fmt.Errorf("can't json marshal argument %v of action %#v: %v", i, selectedAction, err.Error()))
			return
		}

		args = append(args, fmt.Sprintf("%s", string(bt)))
	}

	var selectedMake = -1
	makers := getActionMakers()

	var _makers = make([]string, len(makers))

	for i, mk := range makers {
		_makers[i] = mk[0] + "(" + tview.Escape(mk[1]) + ")"
	}

	for i, f := range makers {
		if f[0] == mt.Make {
			selectedMake = i
		}
	}
	var oldName = mt.Name
	_ = oldName

	/*
		dd := tview.NewDropDown()
		dd.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
			if event.Key() == tcell.KeyEnter {
				return nil
			}
			return event
		})

		dd.SetLabel("Func")
		dd.SetOptions(fns, func(option string, optionIndex int) {
			for i, f := range fns {
				if i == optionIndex {
					mt.Func = f
				}
			}
		})

		dd.SetCurrentOption(selectedFunc)
	*/
	form.
		/*
			AddInputField("Name", mt.Name, 100, nil, func(text string) {
				mt.Name = text
			}).
		*/
		//		AddFormItem(dd).

		AddDropDown("make", _makers, selectedMake, func(option string, optionIndex int) {
			for i, f := range makers {
				if i == optionIndex {
					mt.Make = f[0]
				}
			}
		}).
		AddInputField("parameters (comma separated)", strings.Join(args, ","), 150, func(textToCheck string, lastChar rune) bool {
			/*
				var args []interface{}
				err := json.Unmarshal( []byte("["+textToCheck+"]"), &args)
				return err == nil
			*/
			return true
		}, func(text string) {
			var args []interface{}
			err := json.Unmarshal([]byte("["+text+"]"), &args)
			/*
				if err != nil {
					showError(fmt.Errorf("ERROR in parameters: %s", err.Error()))
				}
			*/
			if err == nil {
				mt.Params = args
			}
		}).
		AddButton("save", func() {
			replaceAction(oldName, mt)
			pages.SwitchToPage("action")
			app.SetFocus(pages)
			//			pagesRight.RemovePage("form")
			//			app.SetFocus(pages)
			//				changeScreen(newTransformerScreen())
			//			changeScreen(runnerPage())
		}).
		SetCancelFunc(func() {
			pages.SwitchToPage("action")
			app.SetFocus(pages)
			app.SetFocus(pagesRight)
		})
	form.SetBorder(true).SetTitle(fmt.Sprintf("edit action %#v", selectedAction)).SetTitleAlign(tview.AlignLeft)
	return form
}

// edit or create a transformer
func actionForm(selectedAction string) (form *tview.Form) {
	if selectedAction == "" {
		return actionFormAdd()
	}
	return actionFormEdit(selectedAction)
}
