package main

import (
	"github.com/rivo/tview"
	lineconfig "gitlab.com/gomidi/midiline/config"
)

var data *lineconfig.Config
var app *tview.Application

var pages *tview.Pages
var pagesRight *tview.Pages
