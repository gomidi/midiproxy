// +build linux

// set USER_DIR based on the XDG Base Directory Specification
// see http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html

package main

import (
	"os"
	"path/filepath"
)

func setUserDir() {
	xdg_config_home := os.Getenv("XDG_CONFIG_HOME")
	if xdg_config_home == "" {
		home := os.Getenv("HOME")
		if home == "" {
			home = filepath.Join("/home", os.Getenv("USER"))
		}
		xdg_config_home = filepath.Join(home, ".midiproxy")
	}
	USER_DIR = xdg_config_home
}

func init() {
	setUserDir()
}
