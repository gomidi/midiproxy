package main

import (
	"fmt"
	"sort"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type actionScreen struct {
	*tview.Box
	currentAction int
	actions       []string
}

func newActionScreen() *actionScreen {
	return &actionScreen{Box: tview.NewBox().SetBorder(true).SetTitle("actions"), actions: getActions(), currentAction: -1}
}

func (m *actionScreen) Draw(screen tcell.Screen) {

	m.Box.Draw(screen)
	x, y, width, height := m.GetInnerRect()

	radioButton := "( )" // Unchecked.
	if m.currentAction == -1 {
		radioButton = "[red](X)" // Checked.
	}
	line := fmt.Sprintf(`%s %s`, radioButton, "NEW")
	tview.Print(screen, line, x+1, y, width, tview.AlignLeft, tcell.ColorYellow)

	for index, option := range m.actions {
		if index >= height-1 {
			break
		}
		radioButton := "( )" // Unchecked.
		if index == m.currentAction {
			radioButton = "[red](X)" // Checked.
		}
		line := fmt.Sprintf(`%s %s`, radioButton, option)
		tview.Print(screen, line, x+2, y+index+1, width, tview.AlignLeft, tcell.ColorYellow)
	}
}

func (m *actionScreen) Focus(delegate func(p tview.Primitive)) {
	m.actions = getActions()
	sort.Strings(m.actions)
	m.showForm()
}

func (m *actionScreen) showForm() {
	form := actionForm(m.currentActionName())
	//changeScreen(form)
	pagesRight.RemovePage("form")
	pagesRight.AddAndSwitchToPage("form", form, true)
}

func (m *actionScreen) currentActionName() string {
	mt := ""
	if m.currentAction >= 0 {
		mt = m.actions[m.currentAction]
	}
	return mt
}

// InputHandler returns the handler for this primitive.
func (m *actionScreen) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	//return m.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		switch event.Key() {
		case tcell.KeyDelete:
			name := m.currentActionName()
			if name != "" {
				removeAction(name)
				m.actions = getActions()
				sort.Strings(m.actions)
				m.currentAction = -1
				m.showForm()
				//app.Refresh()
			}
		case tcell.KeyUp:
			m.currentAction--
			if m.currentAction < -1 {
				m.currentAction = -1
			}
			m.showForm()
		case tcell.KeyDown:
			m.currentAction++
			if m.currentAction >= len(m.actions) {
				m.currentAction = len(m.actions) - 1
			}
			m.showForm()
		default:

		}
	}
	//})
}
