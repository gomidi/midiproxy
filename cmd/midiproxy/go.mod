module gitlab.com/gomidi/midiproxy/cmd/midiproxy

go 1.14

require (
	github.com/gdamore/encoding v0.0.0-20151215212835-b23993cbb635 // indirect
	github.com/gdamore/tcell v1.1.0
	github.com/lucasb-eyer/go-colorful v0.0.0-20180709185858-c7842319cf3a // indirect
	github.com/mattn/go-runewidth v0.0.3 // indirect
	github.com/rivo/tview v0.0.0-20180821142722-77bcb6c6b900
	gitlab.com/gomidi/midi v1.15.3
	gitlab.com/gomidi/midiline v0.16.0
	gitlab.com/gomidi/midiproxy v0.19.0
	gitlab.com/gomidi/rtmididrv v0.9.3
	gitlab.com/metakeule/config v1.13.0
	golang.org/x/text v0.3.0 // indirect
)

replace gitlab.com/gomidi/midiproxy => ../../
