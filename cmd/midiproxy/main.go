package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"

	"encoding/json"
	"sort"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	_ "gitlab.com/gomidi/midiline/actions/channelchange"
	_ "gitlab.com/gomidi/midiline/actions/channelmirror"
	_ "gitlab.com/gomidi/midiline/actions/channelrouter"
	_ "gitlab.com/gomidi/midiline/actions/copy"
	_ "gitlab.com/gomidi/midiline/actions/metronome"
	_ "gitlab.com/gomidi/midiline/actions/outbreak"
	_ "gitlab.com/gomidi/midiline/actions/to_aftertouch"
	_ "gitlab.com/gomidi/midiline/actions/to_cc"
	_ "gitlab.com/gomidi/midiline/actions/to_note"
	_ "gitlab.com/gomidi/midiline/actions/to_pitchbend"
	_ "gitlab.com/gomidi/midiline/actions/to_polyaftertouch"
	_ "gitlab.com/gomidi/midiline/actions/to_programchange"
	_ "gitlab.com/gomidi/midiline/actions/transpose"
	_ "gitlab.com/gomidi/midiline/conditions/logic"
	_ "gitlab.com/gomidi/midiline/conditions/message"
	_ "gitlab.com/gomidi/midiline/conditions/typ"
	lineconfig "gitlab.com/gomidi/midiline/config"
	_ "gitlab.com/gomidi/midiline/value"
	"gitlab.com/gomidi/midiproxy"
	"gitlab.com/gomidi/rtmididrv"
	"gitlab.com/metakeule/config"
)

var USER_DIR string
var CONFIG_FILE string

var (
	cfg = config.MustNew("midiproxy", "0.19.0", "midiproxy performs actions on live MIDI data between a given in and out port")

	configArg = cfg.NewString("conf", "config file that defines the transformation",
		config.Default("config.json"), config.Shortflag('c'))

	listCMD = cfg.MustCommand("ports", "show the MIDI out ports that are available")
	//infoCMD = cfg.MustCommand("info", "show the available Conditions and actions").Relax("in").Relax("out").Relax("line")
	runCMD = cfg.MustCommand("run", "run based on the given config")

	inArg = runCMD.NewInt32("in", "MIDI in port",
		config.Default(int32(0)),
		config.Shortflag('i'),
	)

	outArg = runCMD.NewInt32("out", "MIDI out port",
		config.Default(int32(0)),
		config.Shortflag('o'),
	)

	lineArg = runCMD.NewString("line", "the name of the MIDI line that should be run",
		config.Default("main"),
		config.Shortflag('l'),
	)

	verboseArg = runCMD.NewBool("verbose", "show MIDI messages",
		config.Default(false),
		config.Shortflag('v'),
	)
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stdout, "ERROR: %v\n", err)
	}
}

var drv midi.Driver
var outs []midi.Out
var ins []midi.In

func run() (err error) {
	err = cfg.Run()
	if err != nil {
		return
	}

	drv, err = rtmididrv.New()

	if err != nil {
		return
	}

	ins, err = drv.Ins()

	if err != nil {
		return
	}

	outs, err = drv.Outs()

	if err != nil {
		return
	}

	if cfg.ActiveCommand() == listCMD {
		return showPorts()
	}

	//	if cfg.ActiveCommand() == infoCMD {
	//		return showInfos()
	//	}

	st, err := os.Stat(USER_DIR)
	if err != nil {

		if !os.IsNotExist(err) {
			return fmt.Errorf("unable to find or create config directory at %#v: %v", USER_DIR, err)
		}
		err2 := os.MkdirAll(USER_DIR, 0755)
		if err2 != nil {
			return fmt.Errorf("unable to create config directory at %#v: %v", USER_DIR, err2)
		}
	} else {
		if !st.IsDir() {
			return fmt.Errorf("config directory at %#v is not a directory", USER_DIR)
		}
	}

	CONFIG_FILE = filepath.Join(USER_DIR, configArg.Get())

	if cfg.ActiveCommand() != runCMD {
		return startUI()
	}

	var c *lineconfig.Config
	c, err = readConfig(CONFIG_FILE)

	if err != nil {
		return err
	}

	bt, _ := json.MarshalIndent(c, "", "  ")
	_ = bt
	//	fmt.Printf("%v\n", string(bt))

	/*
		var opts []midiline.PerformOption

		if !verboseArg.Get() {
			opts = append(opts, midiline.PerformReaderOptions(mid.NoLogger()))
		} else {
			opts = append(opts, midiline.LogOutput())
		}
	*/

	var opts []mid.ReaderOption

	if !verboseArg.Get() {
		opts = append(opts, mid.NoLogger())
	} else {
		//			opts = append(opts, midiline.LogOutput())
	}

	in, err := midi.OpenIn(drv, int(inArg.Get()), "")

	if err != nil {
		return err
	}

	out, err := midi.OpenOut(drv, int(outArg.Get()), "")

	if err != nil {
		return err
	}

	var proxy *midiproxy.Proxy
	//proxy, err = midiproxy.New(in, out, c, lineArg.Get(), opts...)
	proxy, err = midiproxy.New(in, out, c, lineArg.Get(), opts...)

	if err != nil {
		return
	}

	fmt.Println("starting...")
	err = proxy.Start()

	if err != nil {
		return
	}

	sigchan := make(chan os.Signal, 10)

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan
	fmt.Print("\ninterrupted, stopping...")
	proxy.Stop()
	os.Exit(0)
	return
}

func readConfig(file string) (*lineconfig.Config, error) {
	var c lineconfig.Config
	bt, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bt, &c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func showInfos() error {
	mt := lineconfig.RegisteredConditionMaker()

	mtinfos := lineconfig.RegisteredConditionMakerInfos()

	sort.Strings(mt)

	fmt.Printf("\n-- Conditions --\n\n")

	for _, name := range mt {
		fmt.Printf("  %s(%s)\n", name, mtinfos[name])
	}

	tr := lineconfig.RegisteredActionMaker()

	sort.Strings(tr)

	trinfos := lineconfig.RegisteredActionMakerInfos()

	fmt.Printf("\n\n-- actions --\n\n")

	for _, name := range tr {
		fmt.Printf("  %s(%s)\n", name, trinfos[name])
	}

	return nil
}

func showPorts() error {
	fmt.Fprintf(os.Stdout, "\nMIDI in ports:\n\n")

	for _, p := range ins {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", p.Number(), p.String())
	}

	fmt.Fprintf(os.Stdout, "\nMIDI out ports:\n\n")

	for _, p := range outs {
		fmt.Fprintf(os.Stdout, "[%v] %s\n", p.Number(), p.String())
	}
	return nil
}

type ioLogger struct {
	io.Writer
}

func (i *ioLogger) Printf(format string, args ...interface{}) {
	fmt.Fprintf(i.Writer, format, args...)
}

func newProxy(wr io.Writer, chosenLine string, chosenInport midi.In, chosenOutport midi.Out) (*midiproxy.Proxy, error) {

	/*
		opts := append([]midiline.PerformOption{},
			midiline.LogOutput(),
			midiline.StdOut(wr),
			midiline.StdErr(wr),
			midiline.PerformReaderOptions(mid.SetLogger(&ioLogger{wr})),
		)
	*/
	return midiproxy.New(chosenInport, chosenOutport, data, chosenLine, mid.SetLogger(&ioLogger{wr}))

}
