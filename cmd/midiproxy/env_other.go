// +build !linux,!windows,!darwin

package main

// environment for unixy system that are not linux and not darwin, like the BSD family

import (
	"os"
	"path/filepath"
)

func setUserDir() {
	home := os.Getenv("HOME")
	if home == "" {
		home = filepath.Join("/home", os.Getenv("USER"))
	}
	USER_DIR = filepath.Join(home + ".midiproxy")
}

func init() {
	setUserDir()
}
