package midiproxy

import (
	"time"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

type Proxy struct {
	runner  midiline.Runner
	line    *midiline.Line
	stop    chan<- bool
	stopped bool
	inPort  midi.In
	outPort midi.Out
}

func (r *Proxy) Start() error {
	r.stopped = false
	stop, err := r.runner.Run(r.line)
	if err != nil {
		r.stopped = true
		return err
	}
	r.stop = stop
	return nil
}

func (r *Proxy) Stop() {
	if r.stopped {
		return
	}
	r.inPort.Close()
	r.outPort.Close()
	r.stop <- true
	r.stopped = true
	time.Sleep(time.Second * 2)
	return
}

//func New(inPort mid.In, outPort mid.Out, c *config.Config, line string, opts ...midiline.PerformOption) (p *Proxy, err error) {
func New(inPort midi.In, outPort midi.Out, c *config.Config, line string, opts ...mid.ReaderOption) (p *Proxy, err error) {
	p = &Proxy{}
	p.outPort = outPort
	p.inPort = inPort
	//p.runner = midiline.Connect(p.inPort, p.outPort, opts...)
	p.runner = midiline.ConnectSlim(p.inPort, p.outPort, opts...)
	p.line, err = c.Line(line)
	return
}
